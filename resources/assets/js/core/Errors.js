/*
* @Author: moshi
* @Date:   2017-08-17 21:18:48
* @Last Modified 2017-08-17
* @Last Modified time: 2017-08-17 21:20:56
*/
class Errors {
	constructor() {
		this.errors = {};
	}

	has(field) {
		// if this.errors contains a "field" property 
		return this.errors.hasOwnProperty(field);
	}

	any() {
		return Object.keys(this.errors).length > 0;
	}

	get(field) {
		if(this.errors[field]) {
			return this.errors[field][0];
		}
	}

	record(errors) {
		this.errors = errors;
	}

	clear(field) {


		if (field)
		{
			delete this.errors[field];
			return;
		} 
		this.errors = {};
	}
}

export default Errors;