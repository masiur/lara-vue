<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CREATE</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.1/css/bulma.min.css">
    <style >
    	body {
    		padding-top: 40px;
    	}
    </style>
  </head>
  <body>
<body>
	<div id="app">
		<div class="container">
		<example></example>
			<form method="POST" action="/projects" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">

				<div class="field">
				  <label class="label" for="name">Project Name</label>
				  <div class="control">
				    <input class="input" id="name" name="name" type="text" v-model="form.name">
				    <span class="help is-danger" v-if="form.errors.has('name')" v-text="form.errors.get('name')"></span>
				  </div>
				</div>

				<div class="field">
				  <label class="label" for="description">Projcet Description</label>
				  <div class="control">
				    <input class="input" id="description" name="description" type="text" v-model="form.description">
				    <span class="help is-danger" v-if="form.errors.has('description')" v-text="form.errors.get('description')"></span>
				  </div>
				</div>

				
					<div class="control">
						<button class="button is-primary" :disabled="form.errors.any()">Create</button>
					</div>
					
				


			</form>
			
		</div>

	</div>

<script src="/js/app.js"></script>


</body>
</html>