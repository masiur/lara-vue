<?php


Route::get('foo', 'FooController@foo');

Route::get('/', function () {

    return view('welcome');
});

Route::get('skills', function () {

    return ['Laravel', 'Vue', 'PHP', 'JavaScript', 'Tooling'];
});

Route::get('projects/create', 'ProjectController@create');
// Route::get('projects', 'ProjectController@index');
Route::post('projects', 'ProjectController@store');

// Route::resource('homes', 'HomesController');












use Illuminate\Support\Facades\App;

Route::get('/bridge', function() {
    $pusher = App::make('pusher');

    $pusher->trigger( 'test-channel',
                      'test-event', 
                      array('text' => 'Preparing the Pusher Laracon.eu workshop!'));

    return view('welcome');
});



Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/check', function() {
	 Artisan::call('route:list');
	 $output = Artisan::output();
	// var_dump($output);
	 dd($output);
});

// Route::get('/', 'PostsController@index');
Route::post('favorite/{post}', 'PostsController@favoritePost');
Route::post('unfavorite/{post}', 'PostsController@unFavoritePost');

Route::get('broadcast', function () {
    event(new \App\Events\StatusLiked('Broadcasting in Laravel using Pusher!'));

    return view('welcome');
});


