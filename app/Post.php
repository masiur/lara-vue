<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Favorite;
use Illuminate\Support\Facades\Auth;
class Post extends Model
{
    protected $fillable = ['user_id', 'content'];
    /**
	 * Determine whether a post has been marked as favorite by a user.
	 *
	 * @return boolean
	 */

	public function favorited()
	{
	    return (bool) Favorite::where('user_id', Auth::id())
	                        ->where('post_id', $this->id)
	                        ->first();
	}

	public function user(){
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getThreadedComments(){
        return $this->comments()->with('user')->get()->threaded();
    }

    public function addComment($attributes)
    {
        $comment = (new Comment())->forceFill($attributes);
        $comment->user_id = auth()->id();
        return $this->comments()->save($comment);
    }
}
