<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\FooRepository;
class FooController extends Controller
{
    
 //    private $repository;
 //    public function __construct(FooRepository $repository)
	// {
	// 	$this->repository = $repository;
	// }

    public function foo(FooRepository $repository) {

    	// method 1 
    	/*
    	// $repository = new \App\Repositories\FooRepository();

    	// return $repository->get();
    	*/
    	//method 2 
    	return $repository->get();
    }
}
